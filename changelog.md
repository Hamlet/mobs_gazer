# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- no other features planned



## [0.1.3] - 2020-08-01
### Changed

	- Code split over different files.
	- Added "nil" checks to avoid crashes.



## [0.1.2] - 2020-05-01
### Changed

	- Fixed wrong "suffocation" setting which caused a bug (thanks Bastrabun).
	- Reduced spawn chance from 7500 to 9000.



## [0.1.1] - 2019-12-10
### Changed

	- Reduced the Gazer's eye size.



## [0.1.0] - 2019-12-07
### Added

	- Initial release.
