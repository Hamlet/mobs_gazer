--[[
	Mobs Gazer - Adds a gazing monster.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Constants
--

local f_NODE_SIZE = 1.0
local f_PLAYER_SIZE = 1.0
local i_DEFAULT_RANGE = 5
local i_ATTEMPTS_LIMIT = 100
local s_ALLOWED_GROUP = 'flora'
local s_ALLOWED_NODE = 'default:snow'


--
-- Functions
--

--[[
	Used to choose a random position around an initial position.
	a_t_position	- table: the initial position.

	The following arguments are optional:
		a_i_offset	- integer: the offset from the initial position.
		a_s_element	- string: the wanted material, default is 'air' if nil.
		a_b_ground	- boolean: wheter if the position must be on ground.
--]]

mobs_gazer.fn_RandomPosition = function(a_t_position, a_i_offset, a_s_element,
	a_b_ground)

	if (a_t_position ~= nil) then

		-- Variables
		local t_CenterPosition = a_t_position
		local t_TargetPosition = nil
		local i_Offset = a_i_offset
		if (i_Offset == nil) then
			i_Offset = i_DEFAULT_RANGE
		end

		local s_WantedElement = a_s_element
		if (s_WantedElement == nil) then
			s_WantedElement = 'air'
		end

		local b_StayOnGround = a_b_ground
		if (b_StayOnGround == nil) then
			b_StayOnGround = true
		end

		local s_ActualMaterial = nil
		local i_IsInAllowedGroup = 0
		local i_Attempts = 0
		local b_DirectLineOfSight = false
		local b_PositionFound = false

		-- Cycle
		while ((i_Attempts < i_ATTEMPTS_LIMIT)
		and (b_PositionFound == false))
		do
			i_Attempts = (i_Attempts + 1)

			if (b_DirectLineOfSight == false) -- Don't 'jump' beyond walls
			or ((s_ActualMaterial ~= s_WantedElement) -- Not air or water
				and ((i_IsInAllowedGroup == 0)  -- Not grass or flower
				and (s_ActualMaterial ~= s_ALLOWED_NODE))) -- Not snow slab
			then
				math.randomseed(os.time())

				local i_OffsetX = math.random(1, i_Offset)
				local i_OffsetZ = math.random(1, i_Offset)
				local i_OffsetY = math.random(1, 3)
				-- Vertical depends on b_StayOnGround

				-- Offsets can be positive or negative.
				if (math.random(1, 100) < 51) then
					i_OffsetX = -i_OffsetX
				end

				if (math.random(1, 100) < 51) then
					i_OffsetZ = -i_OffsetZ
				end

				if (b_StayOnGround == false) then
					i_OffsetY = math.random(1, i_Offset)

					if (math.random(1, 100) < 51) then
						i_OffsetY = -i_OffsetY
					end
				end

				t_TargetPosition = {
					x = (t_CenterPosition.x + i_OffsetX),
					y = (t_CenterPosition.y + i_OffsetY),
					z = (t_CenterPosition.z + i_OffsetZ)
				}

				s_ActualMaterial = minetest.get_node(t_TargetPosition).name
				i_IsInAllowedGroup = minetest.get_item_group(s_ActualMaterial,
					s_ALLOWED_GROUP)

				--print('Name: ' .. s_ActualMaterial)
				--print('Group flora: ' .. i_IsInAllowedGroup)

				b_DirectLineOfSight = minetest.line_of_sight(
					-- Position 1
					{
						x = (t_CenterPosition.x + f_NODE_SIZE),
						y = (t_CenterPosition.y + f_NODE_SIZE),
						z = (t_CenterPosition.z + f_NODE_SIZE)
					},

					-- Position 2
					{
						x = (t_TargetPosition.x + f_PLAYER_SIZE),
						y = (t_TargetPosition.y + f_PLAYER_SIZE),
						z = (t_TargetPosition.z + f_PLAYER_SIZE)
					}
				)

			else
				b_PositionFound = true

			end
		end

		if (b_PositionFound == false) then
			t_TargetPosition = nil

		end

		 --print('\nAttempts: ' .. (i_Attempts - 1) .. '/' .. i_ATTEMPTS_LIMIT)
		 --print('Wanted element: ' .. tostring((s_ActualMaterial == s_WantedElement)))
		 --print('Direct line of sight: ' .. tostring(b_DirectLineOfSight) .. '\n')

		return t_TargetPosition
	end
end


--[[
	Used to calculate the yaw from position 1 to position 2, returns a float.
	a_t_position1 - table: the entity's position.
	a_t_position2 - table: the target's position.

	ATTRIBUTION: This function has been taken from 'Creatures' v1.1.5 Beta
	by BlockMen © 2014, 2015; init.lua at line 183, the License was 'WTFPL'.
--]]

mobs_gazer.fn_LookAtTarget = function(a_t_position1, a_t_position2)
	local t_EntityPosition = a_t_position1
	local t_TargetPosition = a_t_position2

	local t_Vector = {
		x = (t_TargetPosition.x - t_EntityPosition.x),
		y = (t_TargetPosition.y - t_EntityPosition.y),
		z = (t_TargetPosition.z - t_EntityPosition.z)
	}

	local f_Yaw = (math.atan(t_Vector.z / t_Vector.x) + (math.pi ^ 2))

	if (t_TargetPosition.x > t_EntityPosition.x) then
		f_Yaw = (f_Yaw + math.pi)
	end

	f_Yaw = (f_Yaw - 2)

	return f_Yaw
end
