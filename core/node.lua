--[[
	Mobs Gazer - Adds a gazing monster.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Gas node
--

minetest.register_node('mobs_gazer:gas_node', {
	description = 'Gazer\'s gas node',
	groups = {not_in_creative_inventory = 1},
	drawtype = 'glasslike',
	tiles = {
		{
			name = 'default_water_source_animated.png',
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	alpha = 191,
	paramtype = 'light',
	sunlight_propagates = true,
	walkable = false,
	pointable = false,
	diggable = false,
	climbable = false,
	buildable_to = true,
	floodable = false,
	color = '#00FF00',
	post_effect_color = {a = 191, r = 0, g = 100, b = 0},
	light_source = 5,
	damage_per_second = 1,

	on_construct = function(pos)
		minetest.get_node_timer(pos):start(3)
	end,

	on_timer = function(pos, elapsed)
		minetest.set_node(pos, {name = 'air'})
		return true
	end
})
