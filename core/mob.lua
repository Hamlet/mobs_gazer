--[[
	Mobs Gazer - Adds a gazing monster.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Entity
--

mobs:register_mob('mobs_gazer:gazer', {
	--nametag = 'Gazer',
	type = 'monster',
	hp_min = 30,
	hp_max = 40,
	armor = 75,
	walk_velocity = 1,
	run_velocity = 4,
	stand_chance = 25,
	walk_chance = 75,
	jump = true,
	jump_height = 1.1,
	stepheight = 1.1,
	pushable = true,
	view_range = 15,
	damage = 0,
	knock_back = true,
	fear_height = 0,
	fall_damage = false,
	lava_damage = 6,
	suffocation = 0,
	floats = 0,
	reach = 0,
	attack_chance = 1,
	attack_monsters = false,
	attack_animals = true,
	attack_npcs = true,
	attack_players = true,
	group_attack = true,
	attack_type = 'shoot',
	arrow = 'mobs_gazer:gasball',
	shoot_interval = 2,
	shoot_offset = 1.5,
	blood_amount = 0,
	pathfinding = 1,
	makes_footstep_sound = false,
	drops = {
		{name = 'mobs_gazer:gazer_eye',	chance = 8,	min = 1, max = 1}
	},
	visual = 'cube',
	visual_size = {x = 0.4, y = 0.4},
	collisionbox = {-0.2, -0.2, -0.2, 0.2, 0.2, 0.2},
	selectionbox = {-0.2, -0.2, -0.2, 0.2, 0.2, 0.2},
	textures = {
		'mobs_gazer_sides.png',
		'mobs_gazer_sides.png',
		'mobs_gazer_sides.png',
		'mobs_gazer_sides.png',
		'mobs_gazer.png',
		'mobs_gazer_sides.png'
	},

	do_punch = function(self, hitter, time_from_last_punch, tool_capabilities,
		direction)
		local t_TargetPosition = hitter:get_pos()
		local t_TeleportTo = mobs_gazer.fn_RandomPosition(t_TargetPosition)

		if (t_TeleportTo ~= nil) then
			self.object:set_pos(t_TeleportTo)

			local f_Yaw = mobs_gazer.fn_LookAtTarget(self.object:get_pos(),
							t_TargetPosition)

			self.object:set_yaw(f_Yaw)
		end
	end
})
