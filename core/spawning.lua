--[[
	Mobs Gazer - Adds a gazing monster.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Entity spawner
--

mobs:spawn({
	name = 'mobs_gazer:gazer',
	nodes = {
		'group:stone',
		'group:cracky'
	},
	neighbors = {'air'},
	min_light = 0,
	max_light = 5,
	interval = 60,
	chance = 36000,
	active_object_count = 1,
	min_height = -30912,
	max_height = -500
})


-- Spawn Egg

mobs:register_egg('mobs_gazer:gazer', 'Gazer', 'mobs_gazer.png')


--
-- Alias
--

mobs:alias_mob('mobs:gazer', 'mobs_gazer:gazer')
