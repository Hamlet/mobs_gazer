--[[
	Mobs Gazer - Adds a gazing monster.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedures
--

-- Check for free space and place a new node
mobs_gazer.pr_PlaceNode = function(pos)
	local s_OldNodeName = minetest.get_node(pos).name

	if (s_OldNodeName == 'air') then
		minetest.set_node(pos, {name = 'mobs_gazer:gas_node'})
	end
end


-- Create a cube of nodes around the target
mobs_gazer.pr_GasCube = function(pos, offset)
	local t_Coordinates = {y = 0.0, x = 0.0, z = 0.0}

	for i_Value = -offset, offset do
		t_Coordinates.x = (pos.x + i_Value)

		for i_Value = -offset, offset do
			t_Coordinates.z = (pos.z + i_Value)

			for i_Value = -offset, offset do
				t_Coordinates.y = (pos.y + i_Value)
				mobs_gazer.pr_PlaceNode(t_Coordinates)
			end
		end
	end
end
