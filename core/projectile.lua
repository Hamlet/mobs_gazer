--[[
	Mobs Gazer - Adds a gazing monster.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Entity arrow
--

mobs:register_arrow('mobs_gazer:gasball', {
	visual = 'sprite',
	visual_size = {x = 0.125, y = 0.125},
	textures = {'mobs_gazer_projectile.png'},
	velocity = 18,
	tail = 1,
	tail_texture = 'mobs_gazer_projectile.png',
	tail_size = 1.25,
	expire = 0.125,
	glow = 5,

	hit_player = function(self, player)
		if (player ~= nil) then
			player:punch(self.object, 1.0, {
				full_punch_interval = 1.0,
				damage_groups = {fleshy = 1},
			}, nil)

			local v_position = player:get_pos()

			if (v_position ~= nil) then
				mobs_gazer.pr_GasCube(v_position, 1)
			end
		end
	end,

	hit_mob = function(self, player)
		if (player ~= nil) then
			player:punch(self.object, 1.0, {
				full_punch_interval = 1.0,
				damage_groups = {fleshy = 1},
			}, nil)

			local v_position = player:get_pos()

			if (v_position ~= nil) then
				mobs_gazer.pr_GasCube(v_position, 1)
			end
		end
	end,

	hit_node = function(self, pos, node)
		if (pos ~= nil) then
			mobs_gazer.pr_GasCube(pos, 1)
		end
	end
})
